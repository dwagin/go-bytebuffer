package bytealg

import _ "unsafe" // for go:linkname

//go:linkname MakeNoZero internal/bytealg.MakeNoZero
func MakeNoZero(n int) []byte
