package bytebuffer

import (
	"io"
	"sync/atomic"

	"gitlab.com/dwagin/go-bytebuffer/bytealg"
	"gitlab.com/dwagin/go-bytebuffer/runtime"
)

var (
	// Verify ByteBuffer implements the given interfaces.
	_ io.ByteWriter   = new(ByteBuffer)
	_ io.ReaderFrom   = new(ByteBuffer)
	_ io.StringWriter = new(ByteBuffer)
	_ io.Writer       = new(ByteBuffer)
	_ io.WriterTo     = new(ByteBuffer)

	// Verify readCloser implement io.ReadCloser interface.
	_ io.ReadCloser = new(readCloser)
)

// ByteBuffer provides byte buffer, which can be used for minimizing
// memory allocations.
//
// ByteBuffer may be used with functions appending data to the given []byte
// slice. See example code for details.
//
// Use Pool.Get for obtaining an empty ByteBuffer.
type ByteBuffer struct {
	// pool parent bytebuffer.Pool
	pool *Pool

	// B is a byte buffer to use in append-like workloads.
	// See example code for details.
	B []byte
}

// Release return buffer to the bytebuffer.Pool.
//
// The buffer mustn't be accessed after returning to the pool.
func (v *ByteBuffer) Release() {
	if v == nil {
		panic("ByteBuffer: not defined")
	}

	var (
		size = len(v.B)
		idx  int
	)

	if size <= minSize<<9 {
		idx = index1(size)
	} else {
		idx = index2(size)
	}

	shard := runtime.ProcPin() % shards

	if atomic.AddUint32(&v.pool.calls[shard][idx], 1) > calibrateCallsThreshold {
		v.pool.calibrate()
	}

	runtime.ProcUnpin()

	currentMaxSize := int(atomic.LoadUint32(&v.pool.maxSize))
	if cap(v.B) <= currentMaxSize || currentMaxSize == 0 {
		v.Reset()
		v.pool.syncPool.Put(v)
	}
}

// Bytes returns ByteBuffer.B, i.e. all the bytes accumulated in the buffer.
//
// The purpose of this function is bytes.Buffer compatibility.
func (v *ByteBuffer) Bytes() []byte {
	return v.B
}

// Len returns the size of the byte buffer.
func (v *ByteBuffer) Len() int {
	return len(v.B)
}

// Cap returns the capacity of the byte buffer.
func (v *ByteBuffer) Cap() int { return cap(v.B) }

// Truncate discards all but the first n unread bytes from the buffer
// but continues to use the same allocated storage.
//
// It panics if n is negative or greater than the length of the buffer.
func (v *ByteBuffer) Truncate(n int) {
	if n == 0 {
		v.Reset()

		return
	}

	if n < 0 || n > v.Len() {
		panic("ByteBuffer: truncation out of range")
	}

	v.B = v.B[:n]
}

// Reset makes ByteBuffer.B empty.
func (v *ByteBuffer) Reset() {
	v.B = v.B[:0]
}

// Grow grows the buffer's capacity, if necessary, to guarantee space for
// another n bytes.
func (v *ByteBuffer) Grow(n int) {
	if n < 0 {
		panic("ByteBuffer: size cannot be negative")
	}

	l := len(v.B)
	c := cap(v.B)

	if c-l < n {
		old := v.B
		v.B = bytealg.MakeNoZero(l + n)[:l]
		copy(v.B, old)
	}
}

// Write implements io.Writer - it appends p to ByteBuffer.B.
func (v *ByteBuffer) Write(p []byte) (int, error) {
	v.B = append(v.B, p...)
	return len(p), nil
}

// WriteByte appends the byte to the buffer.
//
// The purpose of this function is bytes.Buffer compatibility.
//
// The function always returns nil.
func (v *ByteBuffer) WriteByte(c byte) error {
	v.B = append(v.B, c)
	return nil
}

// WriteString appends s to ByteBuffer.B.
func (v *ByteBuffer) WriteString(s string) (int, error) {
	v.B = append(v.B, s...)
	return len(s), nil
}

// MinRead is the minimum slice size passed to a Read call by ByteBuffer.ReadFrom.
const MinRead = 512

// ReadFrom implements the [io.ReaderFrom] interface.
func (v *ByteBuffer) ReadFrom(r io.Reader) (int64, error) {
	var (
		err   error
		n     int
		total int64
	)

	for {
		v.Grow(MinRead)

		l := len(v.B)

		n, err = r.Read(v.B[l:cap(v.B)])
		if n < 0 {
			panic("ByteBuffer: reader returned negative count from Read")
		}

		v.B = v.B[:l+n]
		total += int64(n)

		if err != nil {
			if err == io.EOF {
				return total, nil
			}

			return total, err //nolint:wrapcheck // transparent wrapper
		}
	}
}

// WriteTo implements the [io.WriterTo] interface.
func (v *ByteBuffer) WriteTo(w io.Writer) (int64, error) {
	l := len(v.B)

	if l == 0 {
		return 0, nil
	}

	m, err := w.Write(v.B)

	if m > l {
		panic("ByteBuffer: invalid Write count")
	}

	n := int64(m)

	if err != nil {
		return n, err //nolint:wrapcheck // transparent wrapper
	}

	// all bytes should have been written, by definition of
	// Write method in io.Writer
	if m != l {
		return n, io.ErrShortWrite
	}

	return n, nil
}

type readCloser struct {
	bb  *ByteBuffer
	off int
}

func (v *readCloser) Read(p []byte) (int, error) {
	if v.off >= v.bb.Len() {
		return 0, io.EOF
	}

	n := copy(p, v.bb.B[v.off:])
	v.off += n

	return n, nil
}

func (v *readCloser) WriteTo(w io.Writer) (int64, error) {
	l := len(v.bb.B) - v.off

	if l == 0 {
		return 0, nil
	}

	m, err := w.Write(v.bb.B[v.off:])

	if m > l {
		panic("ByteBuffer: invalid Write count")
	}

	n := int64(m)

	if err != nil {
		return n, err //nolint:wrapcheck // transparent wrapper
	}

	// all bytes should have been written, by definition of
	// Write method in io.Writer
	if m != l {
		return n, io.ErrShortWrite
	}

	return n, nil
}

func (v *readCloser) Close() error {
	if v.bb != nil {
		v.bb.Release()
		v.bb = nil
	}

	return nil
}

// ReadCloser creates an io.ReadCloser with all the contents of the buffer.
func (v *ByteBuffer) ReadCloser() io.ReadCloser {
	return &readCloser{bb: v, off: 0}
}
