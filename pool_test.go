package bytebuffer //nolint:testpackage // tests non-exported functions

import (
	"errors"
	"fmt"
	"runtime"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestIndex(t *testing.T) {
	testCases := []struct {
		Size  int
		Index int
	}{
		{0, 0},
		{1, 0},
	}

	for i := range steps - 1 {
		testCases = append(testCases, []struct {
			Size  int
			Index int
		}{
			{minSize<<i - 1, i},
			{minSize<<i + 0, i},
			{minSize<<i + 1, i + 1},
		}...)
	}

	testCases = append(testCases, []struct {
		Size  int
		Index int
	}{
		{minSize<<(steps-1) - 1, steps - 1},
		{minSize<<(steps-1) + 0, steps - 1},
		{minSize<<(steps-1) + 1, steps - 1},
		{minSize<<(steps+0) + 0, steps - 1},
		{minSize<<(steps+1) + 0, steps - 1},
		{minSize<<(steps+2) + 0, steps - 1},
	}...)

	for i := range testCases {
		test := testCases[i]

		t.Run(fmt.Sprintf("size=%d index=%d", test.Size, test.Index), func(t *testing.T) {
			require.Equal(t, test.Index, index(test.Size))
		})
	}
}

func TestPool(t *testing.T) {
	t.Run("Calibrate", func(t *testing.T) {
		t.Run("Cleared after calibration", func(t *testing.T) {
			pool := NewPool()
			pool.calls[0][2] = 1000
			pool.calibrate()

			for i := range pool.calls {
				for j := range pool.calls[i] {
					require.Equal(t, uint32(0), pool.calls[i][j])
				}
			}
		})
		t.Run("Correct default size and maximum size", func(t *testing.T) {
			pool := NewPool()
			pool.calls[0][0] = 4000
			pool.calls[0][3] = 4000
			pool.calls[0][5] = 4000
			pool.calls[0][7] = 4000
			pool.calls[0][8] = 3000
			pool.calls[0][9] = 1000
			pool.calibrate()

			require.Equal(t, minSize<<7, int(pool.defSize), "incorrect default size")
			require.Equal(t, minSize<<8, int(pool.maxSize), "incorrect maximum size")
		})
	})
	t.Run("Various sizes", func(t *testing.T) {
		for i := range steps + 1 {
			n := minSize << i

			usePool(t, n-1)
			usePool(t, n)
			usePool(t, n+1)

			for j := range 10 {
				usePool(t, n+j)
			}
		}
	})
	t.Run("Various sizes (concurrent)", func(t *testing.T) {
		maxProcs := runtime.GOMAXPROCS(0)

		ch := make(chan struct{})
		errCh := make(chan error)

		for range maxProcs {
			go func() {
				var err error

				for i := range steps + 1 {
					n := minSize << i

					if err = usePoolConcurrent(n - 1); err != nil {
						errCh <- err

						return
					}

					if err = usePoolConcurrent(n); err != nil {
						errCh <- err

						return
					}

					if err = usePoolConcurrent(n + 1); err != nil {
						errCh <- err

						return
					}

					for j := range 10 {
						if err = usePoolConcurrent(n + j); err != nil {
							errCh <- err

							return
						}
					}
				}

				ch <- struct{}{}
			}()
		}

		for range maxProcs {
			select {
			case <-ch:
			case err := <-errCh:
				t.Fatalf("%v", err)
			case <-time.After(10 * time.Second):
				t.Fatalf("timeout")
			}
		}
	})
}

func usePool(t *testing.T, n int) {
	t.Helper()

	require := require.New(t)

	bb := Get()
	require.NotNil(bb)
	require.Equal(0, bb.Len(), "non-empty byte buffer returned")

	bb.B = allocBytes(bb.B, n)

	bb.Release()
}

func usePoolConcurrent(n int) error {
	bb := Get()
	defer bb.Release()

	if bb == nil {
		return errors.New("error: Expected value not to be nil")
	}

	if bb.Len() != 0 {
		return errors.New("error: Non-empty byte buffer returned")
	}

	bb.B = allocBytes(bb.B, n)

	return nil
}

func allocBytes(bytes []byte, n int) []byte {
	diff := n - cap(bytes)
	if diff <= 0 {
		return bytes[:n]
	}

	return append(bytes, make([]byte, diff)...)
}

func indexOld(size int) int {
	size--
	size >>= minBitSize
	idx := 0

	for size > 0 {
		size >>= 1
		idx++
	}

	if idx >= steps {
		idx = steps - 1
	}

	return idx
}

func BenchmarkIndex(b *testing.B) {
	testCases := make([]int, 0, steps+4)
	for i := range steps + 4 {
		testCases = append(testCases, minSize<<i-1)
	}

	b.Run("Old", func(b *testing.B) {
		for i := range testCases {
			test := testCases[i]
			b.Run(strconv.Itoa(test), func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					_ = indexOld(test)
				}
			})
		}
	})
	b.Run("New", func(b *testing.B) {
		for i := range testCases {
			test := testCases[i]
			b.Run(strconv.Itoa(test), func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					if test <= minSize<<9 {
						_ = index1(test)
					} else {
						_ = index2(test)
					}
				}
			})
		}
	})
}

func BenchmarkCalibrate(b *testing.B) {
	pool := NewPool()

	b.ReportAllocs()
	b.ResetTimer()

	for range b.N {
		pool.calls[0][2] = 1000
		pool.calls[0][5] = 1000
		pool.calls[0][12] = 1000
		pool.calls[0][15] = 1000
		pool.calibrate()
	}
}
