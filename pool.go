package bytebuffer

import (
	"slices"
	"sync"
	"sync/atomic"
	"unsafe"

	"gitlab.com/dwagin/go-bytebuffer/bytealg"
)

const (
	shards = 15
	steps  = 20

	minBitSize = 7
	minSize    = 1 << minBitSize // CPU cache line size

	calibrateCallsThreshold = 10000

	minPercentile = 0.8
	maxPercentile = 0.95
)

// Pool represents byte buffer pool.
//
// Distinct pools may be used for distinct types of byte buffers.
// Properly determined byte buffer types with their own pools may help reducing
// memory waste.
type Pool struct {
	syncPool sync.Pool

	defSize uint32
	maxSize uint32

	mu sync.Mutex

	pad [72]byte //nolint:unused // padding

	calls [shards][steps + 12]uint32
}

var defaultPool = NewPool()

// NewPool returns a properly initialized Pool.
func NewPool() *Pool {
	return &Pool{defSize: minSize}
}

// Get returns an empty byte buffer from the default Pool.
//
// Got ByteBuffer may be returned to the pool via ByteBuffer.Release call.
// This reduces the number of memory allocations required for byte buffer
// management.
func Get() *ByteBuffer { return defaultPool.Get() }

// Get returns an empty byte buffer.
//
// Got byte buffer may be returned to the pool via ByteBuffer.Release call.
// This reduces the number of memory allocations required for byte buffer
// management.
func (v *Pool) Get() *ByteBuffer {
	if item := v.syncPool.Get(); item != nil {
		return item.(*ByteBuffer) //nolint:errcheck,forcetypeassert // redundant
	}

	return &ByteBuffer{
		pool: v, B: bytealg.MakeNoZero(int(atomic.LoadUint32(&v.defSize)))[:0],
	}
}

type callItem struct {
	count uint32
	size  uint32
}

func callItemSort(a, b callItem) int {
	switch {
	case a.count > b.count:
		return -1
	case a.count < b.count:
		return 1
	case a.size > b.size:
		return -1
	case a.size < b.size:
		return 1
	default:
		return 0
	}
}

func (v *Pool) calibrate() {
	if !v.mu.TryLock() {
		return
	}

	all := uint32(0)
	calls := make([]callItem, steps)

	for i := range steps {
		calls[i].size = minSize << i
	}

	if littleEndian {
		for i := range v.calls {
			for j := 0; j < steps; j += 4 {
				first := atomic.SwapUint64((*uint64)(unsafe.Pointer(&v.calls[i][j+0])), 0)
				second := atomic.SwapUint64((*uint64)(unsafe.Pointer(&v.calls[i][j+2])), 0)

				count0 := uint32(first)        //nolint:gosec // safe conversion
				count1 := uint32(first >> 32)  //nolint:gosec // safe conversion
				count2 := uint32(second)       //nolint:gosec // safe conversion
				count3 := uint32(second >> 32) //nolint:gosec // safe conversion

				all += count0 + count1 + count2 + count3

				calls[j+0].count += count0
				calls[j+1].count += count1
				calls[j+2].count += count2
				calls[j+3].count += count3
			}
		}
	} else {
		for i := range v.calls {
			for j := 0; j < steps; j += 4 {
				first := atomic.SwapUint64((*uint64)(unsafe.Pointer(&v.calls[i][j+0])), 0)
				second := atomic.SwapUint64((*uint64)(unsafe.Pointer(&v.calls[i][j+2])), 0)

				count0 := uint32(first >> 32)  //nolint:gosec // safe conversion
				count1 := uint32(first)        //nolint:gosec // safe conversion
				count2 := uint32(second >> 32) //nolint:gosec // safe conversion
				count3 := uint32(second)       //nolint:gosec // safe conversion

				all += count0 + count1 + count2 + count3

				calls[j+0].count += count0
				calls[j+1].count += count1
				calls[j+2].count += count2
				calls[j+3].count += count3
			}
		}
	}

	slices.SortFunc(calls, callItemSort)

	minCalls := uint32(float32(all) * minPercentile)
	maxCalls := uint32(float32(all) * maxPercentile)

	all = 0
	newDefSize := uint32(0)

	for i := range calls {
		if all >= minCalls {
			break
		}

		newDefSize = max(newDefSize, calls[i].size)
		all += calls[i].count
	}

	all = 0
	newMaxSize := newDefSize

	for i := range calls {
		if all >= maxCalls {
			break
		}

		newMaxSize = max(newMaxSize, calls[i].size)
		all += calls[i].count
	}

	atomic.StoreUint32(&v.defSize, newDefSize)
	atomic.StoreUint32(&v.maxSize, newMaxSize)

	v.mu.Unlock()
}

func index(size int) int {
	if size <= minSize<<9 {
		return index1(size)
	}

	return index2(size)
}

func index1(size int) int {
	switch {
	case size <= minSize<<4:
		switch {
		case size <= minSize<<0:
			return 0
		case size <= minSize<<1:
			return 1
		case size <= minSize<<2:
			return 2
		case size <= minSize<<3:
			return 3
		default:
			return 4
		}
	case size <= minSize<<9:
		switch {
		case size <= minSize<<5:
			return 5
		case size <= minSize<<6:
			return 6
		case size <= minSize<<7:
			return 7
		case size <= minSize<<8:
			return 8
		default:
			return 9
		}
	default:
		return 0
	}
}

func index2(size int) int {
	switch {
	case size <= minSize<<14:
		switch {
		case size <= minSize<<10:
			return 10
		case size <= minSize<<11:
			return 11
		case size <= minSize<<12:
			return 12
		case size <= minSize<<13:
			return 13
		default:
			return 14
		}
	case size <= minSize<<18:
		switch {
		case size <= minSize<<15:
			return 15
		case size <= minSize<<16:
			return 16
		case size <= minSize<<17:
			return 17
		default:
			return 18
		}
	default:
		return steps - 1
	}
}
