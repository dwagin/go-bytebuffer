package bytebuffer_test

import (
	"testing"

	"gitlab.com/dwagin/go-bytebuffer"
)

func TestExample(t *testing.T) {
	bb := bytebuffer.Get()
	bb.Write([]byte("first line\n"))
	bb.WriteByte(byte('b'))
	bb.WriteByte(byte('y'))
	bb.WriteByte(byte('t'))
	bb.WriteByte(byte('e'))
	bb.WriteByte(byte(0x0a))
	bb.WriteString("second line\n")
	bb.B = append(bb.B, "third line\n"...)

	t.Logf("bytebuffer content=%q", bb.B)

	// It is safe to release byte buffer now, since it is
	// no longer used.
	bb.Release()
}
