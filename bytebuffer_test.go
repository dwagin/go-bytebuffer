package bytebuffer_test

import (
	"bytes"
	"crypto/rand"
	"io"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/valyala/bytebufferpool"

	"gitlab.com/dwagin/go-bytebuffer"
)

var otherPool = sync.Pool{
	New: func() any {
		bb := new(bytes.Buffer)
		bb.Grow(128)

		return bb
	},
}

func TestByteBuffer(t *testing.T) {
	const testSize = 100 * 1024

	t.Run("Grow", func(t *testing.T) {
		require := require.New(t)

		bb := bytebuffer.NewPool().Get()
		bb.B = nil

		bb.Grow(1000)
		require.GreaterOrEqual(bb.Cap()-bb.Len(), 1000)

		bb.Grow(2000)
		require.GreaterOrEqual(bb.Cap()-bb.Len(), 2000)
	})
	t.Run("ReadFrom", func(t *testing.T) {
		require := require.New(t)

		src := make([]byte, testSize)

		n, err := rand.Read(src)
		require.NoError(err)
		require.Equal(testSize, n)

		dst := bytebuffer.NewPool().Get()
		dst.B = nil // Grow

		n2, err := dst.ReadFrom(bytes.NewReader(src))
		require.NoError(err)
		require.Equal(testSize, int(n2))
		require.Equal(src, dst.Bytes())
	})
	t.Run("WriteTo", func(t *testing.T) {
		require := require.New(t)

		src := bytebuffer.NewPool().Get()
		src.Grow(testSize)
		src.B = src.B[:testSize]

		n, err := rand.Read(src.B)
		require.NoError(err)
		require.Equal(testSize, n)

		dst := &bytes.Buffer{}
		dst.Grow(testSize)

		n2, err := src.WriteTo(dst)
		require.NoError(err)
		require.Equal(testSize, int(n2))
		require.Equal(src.Bytes(), dst.Bytes())
	})
	t.Run("ReadCloser", func(t *testing.T) {
		t.Run("Read", func(t *testing.T) {
			require := require.New(t)

			src := bytebuffer.NewPool().Get()
			src.Grow(testSize)
			src.B = src.B[:testSize]

			n, err := rand.Read(src.B)
			require.NoError(err)
			require.Equal(testSize, n)

			rc := src.ReadCloser()
			defer rc.Close()

			dst := make([]byte, testSize)

			n, err = io.ReadFull(rc, dst)
			require.NoError(err)
			require.Equal(testSize, n)
			require.Equal(src.Bytes(), dst)
		})
		t.Run("Copy", func(t *testing.T) {
			require := require.New(t)

			src := bytebuffer.NewPool().Get()
			src.Grow(testSize)
			src.B = src.B[:testSize]

			n, err := rand.Read(src.B)
			require.NoError(err)
			require.Equal(testSize, n)

			rc := src.ReadCloser()
			defer rc.Close()

			dst := bytebuffer.NewPool().Get()
			dst.Grow(testSize / 3)
			dst.B = dst.B[:testSize/3]

			n, err = rc.Read(dst.B)
			require.NoError(err)
			require.Equal(len(dst.B), n)

			n2, err := io.Copy(dst, rc)
			require.NoError(err)
			require.Equal(testSize-testSize/3, int(n2))
			require.Equal(src.Bytes(), dst.Bytes())
		})
	})
}

func getBytesBuffer() *bytes.Buffer {
	return otherPool.Get().(*bytes.Buffer) //nolint:errcheck,forcetypeassert // redundant
}

func putBytesBuffer(bb *bytes.Buffer) {
	bb.Reset()
	otherPool.Put(bb)
}

func BenchmarkSmallWrite(b *testing.B) {
	data := []byte("foo-bar-baz")

	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			bb := bytebuffer.Get()
			bb.Write(data)
			bb.Write(data)
			bb.Write(data)
			bb.Release()
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				bb := bytebuffer.Get()
				bb.Write(data)
				bb.Write(data)
				bb.Write(data)
				bb.Release()
			}
		})
	})
	b.Run("Valyala", func(b *testing.B) {
		b.Run("Serial", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for range b.N {
				bb := bytebufferpool.Get()
				bb.Write(data) //nolint:errcheck // benchmark
				bb.Write(data) //nolint:errcheck // benchmark
				bb.Write(data) //nolint:errcheck // benchmark
				bytebufferpool.Put(bb)
			}
		})

		b.Run("Concurrent", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					bb := bytebufferpool.Get()
					bb.Write(data) //nolint:errcheck // benchmark
					bb.Write(data) //nolint:errcheck // benchmark
					bb.Write(data) //nolint:errcheck // benchmark
					bytebufferpool.Put(bb)
				}
			})
		})
	})
	b.Run("bytes.Buffer", func(b *testing.B) {
		b.Run("Serial", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for range b.N {
				bb := getBytesBuffer()
				bb.Write(data)
				bb.Write(data)
				bb.Write(data)
				putBytesBuffer(bb)
			}
		})

		b.Run("Concurrent", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					bb := getBytesBuffer()
					bb.Write(data)
					bb.Write(data)
					bb.Write(data)
					putBytesBuffer(bb)
				}
			})
		})
	})
}

func BenchmarkLargeWrite(b *testing.B) {
	data := []byte("foo-bar-baz-foo-bar-baz-foo-bar-baz-foo-bar-baz-foo-bar-baz")

	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			bb := bytebuffer.Get()

			for range 20 {
				bb.Write(data)
			}

			bb.Release()
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				bb := bytebuffer.Get()

				for range 20 {
					bb.Write(data)
				}

				bb.Release()
			}
		})
	})
	b.Run("Valyala", func(b *testing.B) {
		b.Run("Serial", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for range b.N {
				bb := bytebufferpool.Get()

				for range 20 {
					bb.Write(data) //nolint:errcheck // benchmark
				}

				bytebufferpool.Put(bb)
			}
		})

		b.Run("Concurrent", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					bb := bytebufferpool.Get()

					for range 20 {
						bb.Write(data) //nolint:errcheck // benchmark
					}

					bytebufferpool.Put(bb)
				}
			})
		})
	})
	b.Run("bytes.Buffer", func(b *testing.B) {
		b.Run("Serial", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			for range b.N {
				bb := getBytesBuffer()

				for range 20 {
					bb.Write(data)
				}

				putBytesBuffer(bb)
			}
		})

		b.Run("Concurrent", func(b *testing.B) {
			b.ReportAllocs()
			b.ResetTimer()

			b.RunParallel(func(pb *testing.PB) {
				for pb.Next() {
					bb := getBytesBuffer()

					for range 20 {
						bb.Write(data)
					}

					putBytesBuffer(bb)
				}
			})
		})
	})
}
